import home from "./assets/img/home.svg";
import phone from "./assets/img/phone.svg";
import mail from "./assets/img/mail.svg";
import oval from "./assets/icon/Oval.svg";
import dotShape from "./assets/icon/Dotted Shape.svg";

export const images = {
    home: home,
    phone: phone,
    mail: mail,
    oval: oval,
    dotShape: dotShape,
};
