export const BlockLeft = (images) => {
    return(
        <div className="w-[570px]">
                <span className="font-[600] text-[16px] leading-[20px] text-[#3056D3]">Contact Us</span>
                <h1 className="font-[700] text-[40px] leading-[40px] mt-[12px] text-[#212B36]">Get In Touch With Us</h1>
                <p className="mt-[33px] font-[400px] text-[16px] leading-[27px] text-[#637381]">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius tempor incididunt ut labore et dolore magna aliqua. Ut enim adiqua minim veniam quis nostrud exercitation ullamco</p>
                <div className="flex items-center mt-[40px]">
                    <img src={images.home} alt="" />
                        <div className="w-[231px] ms-[24px]">
                            <h2 className="font-[600] text-[20px] leading-[27px]">Our Location</h2>
                            <p className="mt-[6px]">99 S.t Jomblo Park Pekanbaru 28292. Indonesia</p>
                        </div>
                </div>
                <div className="flex items-center mt-[35px]">
                    <img src={images.phone} alt="" />
                    <div className="w-[231px] ms-[24px]">
                        <h2 className="font-[600] text-20px leading-[27px]">Phone Number</h2>
                        <p className="mt-[6px]">(+62)81 414 257 9980</p>
                    </div>
                </div>
                <div className="flex items-center mt-[35px]">
                    <img src={images.mail} alt="" />
                    <div className="w-[231px] ms-[24px]">
                        <h2 className="font-[600] text-20px leading-[27px]">Email Address</h2>
                        <p className="mt-[6px]">info@yourdomain.com</p>
                    </div>
                </div>
            </div>
    )
}