export const BlockRight = (images) => {
    return (
        <div className="relative ms-[130px] mt-[13px]">
            <img className="absolute top-[-54px] right-[-50px] z-[1]" src={images.oval} alt="" />
            <img className="absolute top-[63px] right-[-40.67px] z-[1]" src={images.dotShape} alt="" />
            <img className="absolute bottom-[-21.67px] left-[-21px] z-[1]" src={images.dotShape} alt="" />
            <div className="Rectangle4431 w-[470px] h-[530px] bg-white rounded-lg shadow p-[50px] relative z-[1]">
                <input  className="text-[#637381] Rectangle4432 w-[370px] h-[50px] bg-white rounded-[5px] border border-zinc-100" type="text" defaultValue={"Your Name"} name="" id="" />
                <input  className="text-[#637381] mt-[25px] Rectangle4432 w-[370px] h-[50px] bg-white rounded-[5px] border border-zinc-100" defaultValue={"Your Email"} type="text" name="" id="" />
                <input className="text-[#637381] mt-[25px] Rectangle4432 w-[370px] h-[50px] bg-white rounded-[5px] border border-zinc-100" type="text" defaultValue={"Your Phone"} name="" id="" />
                <textarea rows="5" className="text-[#637381] mt-[25px] Rectangle4432 w-[370px] h-[50px] bg-white rounded-[5px] border border-zinc-100" type="text-area" defaultValue={"Your Message"} id="" />
                <button className="mt-[25px] border bg-blue-700 text-[#FFF] min-w-full text-center pt-[15px] pb-[15px] rounded-[5px]">Send Message</button>
            </div>
        </div>
    )
}