import { images } from "../data"
import { BlockLeft } from "./BlockLeft"
import { BlockRight } from "./BlockRight"

export const Contact = () => {
    return (
        <div className="flex mt-[93px]">
            <BlockLeft images={images}/>
            <BlockRight images={images}/>
        </div>
    )
}